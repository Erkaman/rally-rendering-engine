# rally-rendering-engine

This project was about making a rendering engine for a rally game.

The main() function of the engine, can be found in samples/game/src/main.cpp

I got some videos of the engine here:

https://www.youtube.com/watch?v=rxl2BbrK32o
https://www.youtube.com/watch?v=Ppm4iTrzOvs

The code in deps/ is dependencies, and not my code. Everything else is
my code.
