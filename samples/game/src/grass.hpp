#pragma once

#include "ewa/gl/gl_common.hpp"

#include "ewa/math/vector2i.hpp"
#include "ewa/math/vector2f.hpp"
#include "ewa/math/vector3f.hpp"
#include "ewa/mult_array.hpp"
#include "ewa/aabb.hpp"


#include "ewa/random.hpp"

#include <map>
#include <memory>

class VBO;
class ShaderProgram;
class ICamera;
class Texture;
class Vector2f;
class Vector3f;
class Vector4f;
class HeightMap;
class Vector2i;
class Texture2D;
class ViewFrustum;
class DualParaboloidMap;
class Paraboloid;

class Grass{

private:
// a single grass object.
    struct GrassInfo {
    public:
        Vector2f pos;
        float angle;
        float size;

        int baseIndex;
    };

    //  grass tile, affected by turbulent wind.
    // these are seprate from the chunks.
    struct GrassTile {
    public:

	Vector3f m_dir;
	Vector3f m_v;

	float m_xDelta;
	float m_zDelta;

	GrassTile(Random& rng);

	void Update(const float delta);
    };

    //chunks of grass objects. instead of drawing all grasses with a draw call each,
    // we put the grasses into chunks, and draw each chunk with a single draw call.
    struct GrassChunk {
    public:
	unsigned int m_grassNumTriangles;

        std::unique_ptr<VBO> m_grassVertexBuffer;
        std::unique_ptr<VBO> m_grassIndexBuffer;

	FloatVector m_grassVertices;
	std::vector<GLuint> m_grassIndices;
    };

    std::unique_ptr<ShaderProgram> m_deferredShader;
    std::unique_ptr<ShaderProgram> m_reflectionShader;
    std::unique_ptr<ShaderProgram> m_outputIdShader;
    std::unique_ptr<ShaderProgram> m_envMapShader;

    std::unique_ptr<Texture> m_grassTexture;
    std::unique_ptr<HeightMap> m_heightMap;

    // texture that stores turbulent wind.
    std::unique_ptr<Texture2D> m_turbWindTexture;
    MultArray<Vector3f>* m_turbWindTextureBuffer;

    int m_currentId;
    Random m_rng;
    std::map<int,GrassInfo> m_grass;


    float m_heightMapResolution;
    float m_chunkSize;
    float m_time;

    Vector2f m_cameraPosition;
    Vector3f m_cameraDir;
    float m_xzScale;

    MultArray<AABB>* m_aabbs;

    std::unique_ptr<Texture2D> m_meanWindTexture;
    MultArray<Vector3f>* m_meanWindTextureBuffer;
    MultArray<Vector3f>* m_blowWindTextureBuffer;

    MultArray<GrassTile*>* m_turbWindField;

    MultArray<GrassChunk*>* m_chunks;

    std::vector<Vector2i>* m_inCameraFrustum;
    std::vector<Vector2i>* m_inReflectionFrustum;
    std::vector<Vector2i>* m_inEnvFrustums[2];

    bool m_doWind;

    Vector3f m_windCenter;
    float m_windRadius;

    void GenerateGrassVertices(const Vector2f position, const float angle, const float width, const float height, int id);

    void MakeGrass(const Vector2f position, const float angle, const float width, const float height, int id);

    void Draw(const ICamera* camera, const Vector4f& lightPosition, ShaderProgram* shader,
	      const std::vector<Vector2i>& inFrustum, Paraboloid* paraboloid);

    void Rebuild();

    void Init(HeightMap* heightMap);

    void UpdateWind(const float delta);

    void CreateAABBs(const float yScale, const Vector3f& offset, float xzScale);

    Vector3f GetChunkCornerPos(int chunkX, int chunkZ, float y, const Vector3f& offset, float xzScale);

public:

    Grass(const std::string& filename, HeightMap* heightMap);
    Grass(HeightMap* heightMap);

    ~Grass();

    void DrawDeferred(const ICamera* camera, const Vector4f& lightPosition);
    void DrawReflection(const ICamera* camera, const Vector4f& lightPosition);

    // draw into environment map.
    void DrawEnvMap(Paraboloid* paraboloid, const Vector4f& lightPosition, int i);

    void RenderIdAll(const ICamera* camera);

    void Update(const float delta, const Vector2f& cameraPosition, const Vector3f& cameraDir,
		const ViewFrustum& cameraFrustum, const ViewFrustum& lightFrustum,
		DualParaboloidMap& dualParaboloidMap, const ViewFrustum& reflectionFrustum);

    // add grass at position.
    void AddGrass(const Vector2i& position, int grassClusterSize);

    void RemoveGrass(int id);

    void SaveGrass(const std::string& filename); // save grass positions to file.

    GLuint GetBaseIndex(FloatVector& grassVertices);

    void BlowWind();

};
