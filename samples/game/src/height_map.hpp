
#pragma once

#include <string>
#include "math/color.hpp"
#include "math/vector4f.hpp"
#include "math/vector2f.hpp"

#include "ewa/mult_array.hpp"
#include "ewa/aabb.hpp"
#include "math/vector2i.hpp"

#include "ewa/gl/gl_common.hpp"
#include "ewa/gl/texture2d.hpp"
#include "ewa/gl/shader_program.hpp"

#include "ewa/log.hpp"
#include "gui_enum.hpp"

#include "math/vector4f.hpp"

#include "dual_paraboloid_map.hpp"


class VBO;
class ShaderProgram;
class ICamera;
class Texture;
class PerlinSeed;
class Texture;
class ShaderProgra;
class Config;
class ICamera;
class PhysicsWorld;
class ValueNoise;
class DepthFBO;
class Matrix4f;
class ViewFrustum;
class Cube;
class ViewFrustum;
class Paraboloid;
class DualParaboloidMap;

template<typename T>
class PBO {
private:
    int m_updateCount; // how many more updates are necesssary.

    std::unique_ptr<Texture2D> m_texture;
    MultArray<T> *m_textureData;
    const int TEXTURE_SIZE;

    unsigned int m_pboIds[2];

public:
    PBO(Texture2D* texture, MultArray<T> *textureData, const int textureSize):
	TEXTURE_SIZE(textureSize){
	m_texture.reset(texture);
	m_textureData = textureData;

	RequestUpdate(); // need some initial updates to properly setup the PBOs

        GL_C(glGenBuffers(2, m_pboIds));
        GL_C(glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_pboIds[0]));
        GL_C(glBufferData(GL_PIXEL_UNPACK_BUFFER,TEXTURE_SIZE,0, GL_STREAM_DRAW));
        GL_C(glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_pboIds[1]));
        GL_C(glBufferData(GL_PIXEL_UNPACK_BUFFER,TEXTURE_SIZE,0, GL_STREAM_DRAW));
        GL_C(glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0));
    }

    ~PBO() {
        glDeleteBuffers(2, m_pboIds);
    }

    void RequestUpdate() {
	m_updateCount = 2;
    }

    void Update() {

	static int index = 0;
	int nextIndex = 0;

	if(m_updateCount > 0) {

	    index = (index + 1) % 2;
	    nextIndex = (index + 1) % 2;

	    m_texture->Bind();


	    GL_C(glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_pboIds[index] ));

	    m_texture->UpdateTexture(0);


	    GL_C(glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0));

	    m_texture->Unbind();

	    // bind PBO to update texture source
	    GL_C(glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_pboIds[nextIndex] ));

	    GL_C(glBufferData(GL_PIXEL_UNPACK_BUFFER, TEXTURE_SIZE, 0, GL_STREAM_DRAW));

	    GLubyte* ptr = (GLubyte*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER,
                                                 GL_WRITE_ONLY);

	    if(ptr) {
		memcpy(ptr, m_textureData->GetData(), TEXTURE_SIZE );

		GL_C(glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER)); // release the mapped buffer
	    }

	    GL_C(glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0));

	    --m_updateCount;
	}
    }


};

// the data associated with every triangle in the heightmap mesh.
struct Cell {
    Vector2f position; // specifies vertex x and z!
};

struct SplatColor{
public:
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
};

enum HeightMapRenderMode {

    HEIGHT_MAP_RENDER_MODE_ENV_MAP0,
    HEIGHT_MAP_RENDER_MODE_ENV_MAP1,
/*    HEIGHT_MAP_RENDER_MODE_ENV_MAP2,
      HEIGHT_MAP_RENDER_MODE_ENV_MAP3,
      HEIGHT_MAP_RENDER_MODE_ENV_MAP4,
      HEIGHT_MAP_RENDER_MODE_ENV_MAP5,
*/
    HEIGHT_MAP_RENDER_MODE_NORMAL,
    HEIGHT_MAP_RENDER_MODE_SHADOWS,
    HEIGHT_MAP_RENDER_MODE_REFLECTION,
    HEIGHT_MAP_RENDER_MODE_REFRACTION,
    HEIGHT_MAP_RENDER_MODE_COUNT,
};

/*
Heightmap renderer. Also supports editing of the heightmap.
 */
class HeightMap {
private:

    // the heightmap is partitioned into chunks.
    struct Chunk {
	VBO* m_vertexBuffer;
	VBO* m_indexBuffer;
	unsigned int m_numTriangles;
    };


    int m_guiMode;

    unsigned int m_numTriangles;
    bool m_isWireframe;

    Chunk* m_chunkVersions[HEIGHT_MAP_RENDER_MODE_COUNT];

    std::unique_ptr<VBO> m_cursorVertexBuffer;

    unsigned short m_numCursorPoints;

    std::unique_ptr<ShaderProgram> m_shader;
    std::unique_ptr<ShaderProgram> m_depthShader; //outputs only the depth. Used for shadow mapping.
//    ShaderProgram* m_idShader; //outputs only the id. Used for triangle picking in the height map.
    std::unique_ptr<ShaderProgram> m_cursorShader;
    std::unique_ptr<ShaderProgram> m_envShader;
    std::unique_ptr<ShaderProgram> m_refractionShader;
    std::unique_ptr<ShaderProgram> m_reflectionShader;
    std::unique_ptr<ShaderProgram> m_cubeShader;

    std::unique_ptr<Texture> m_grassTexture;
    std::unique_ptr<Texture> m_dirtTexture;
    std::unique_ptr<Texture> m_rockTexture;
    std::unique_ptr<Texture> m_asphaltTexture;

    std::unique_ptr<Texture2D> m_heightMap;
    std::unique_ptr<Texture2D> m_splatMap;
    std::unique_ptr<Texture2D> m_aoMap;

    MultArray<unsigned short>* m_heightData;
    MultArray<SplatColor>* m_splatData;

    MultArray<float>* m_aoData;

    MultArray<AABB>* m_aabbs;// the AABBs of the chunks.
    Cube* m_aabbWireframe;

    std::vector<Vector2i>* m_inCameraFrustum;
    std::vector<Vector2i>* m_inLightFrustum;
    std::vector<Vector2i>* m_inReflectionFrustum;

    std::vector<Vector2i>* m_inEnvFrustums[2];

    // used to store temp data in SmoothTerrain()
    MultArray<unsigned short>* m_tempData;

    Config* m_config;

    Vector3f m_offset;
    float m_xzScale;
    float m_yScale;
    int m_resolution;
    int m_chunks; // how many chunks to subdivide the mesh into, on one axis.
    int m_chunkSize; // how many quads per chunk width.

    float m_textureScale;
    int HEIGHT_MAP_SIZE;
    int SPLAT_MAP_SIZE;

    Vector2i m_cursorPosition;
    bool m_cursorPositionWasUpdated;
    int m_cursorSize;

    ValueNoise* m_noise;

    std::unique_ptr<PBO<unsigned short>> m_heightMapPbo;
    std::unique_ptr<PBO<SplatColor>> m_splatMapPbo;

    std::unique_ptr<VBO> m_cubeIndexBuffer;
    std::unique_ptr<VBO> m_cubePositionBuffer;
    GLushort m_cubeNumIndices;

    std::vector<Vector2i> m_controlPoints;

    static const float ComputeY(const unsigned char heightMapData );
    static const float ScaleXZ(const int x);
    static const Color VertexColoring(const float y);

    Chunk* CreateChunk(float scaling);

    void CreateHeightmap(const std::string& heightMapFilename, bool guiMode);
    void CreateCursor();
    void CreateCube();
    void CreateSplatMap(const std::string& splatMapFilename, bool guiMode);
    void CreateAABBs();

    void LoadHeightmap(const std::string& heightMapFilename);
    void LoadSplatMap(const std::string& splatMapFilename);

    void RenderHeightMap(
	const ICamera* camera, const Vector4f& lightPosition, const Matrix4f& lightVp, const DepthFBO& shadowMap,
	const bool aoOnly);
    void RenderCursor(const ICamera* camera);

    void RenderCubeCursor(const ICamera* camera, float cubeScale);

    void Render(ShaderProgram* shader, HeightMapRenderMode renderMode);

    void RenderSetup(ShaderProgram* shader);
    void RenderUnsetup();

    void UpdateCursor(ICamera* camera,
		      const float framebufferWidth,
		      const float framebufferHeight);

    void Init(
	const std::string& heightMapFilename,
	const std::string& splatMapFilename,
	const std::string& aoMapFilename,
	bool guiMode );

    bool InBounds(int x, int z);

    Vector3f GetChunkCornerPos(int chunkX, int chunkZ, float y);

public:

    HeightMap(
	const std::string& heightMapFilename,
	const std::string& splatMapFilename,
	const std::string& aoMapFilename,
	bool guiMode );
    HeightMap(bool guiMode );


    ~HeightMap();

    void Render(const ICamera* camera, const Vector4f& lightPosition, const Matrix4f& lightVp, const DepthFBO& shadowMap, const bool aoOnly);

    void RenderShadowMap(const Matrix4f& lightVp);

    void RenderParaboloid(const Paraboloid& paraboloid, const Vector4f& lightPosition, bool aoOnly);

    void RenderRefraction(
	const ICamera* camera, const Vector4f& lightPosition, bool aoOnly);

    void RenderReflection(
	const ICamera* camera, const Vector4f& lightPosition, bool aoOnly);

    void SetWireframe(const bool wireframe);

    // need to be updated every frame if there is a gui.
    void UpdateGui(const float delta, ICamera* camera,
                   const float framebufferWidth,
                   const float framebufferHeight
	);

    void Update(const ViewFrustum& cameraFrustum, const ViewFrustum& lightFrustum,
		ViewFrustum** envLightFrustums,
		const ViewFrustum& reflectionFrustum,
		DualParaboloidMap& dualParaboloidMap);



    void ModifyTerrain(const float delta, const float strength);
    void DistortTerrain(const float delta, const float strength, float noiseScale);
    void SmoothTerrain(const float delta, const int smoothRadius);
    void LevelTerrain(const float delta, const float strength);
    void ErodeTerrain();

    void DrawTexture(const float delta, int drawTextureType);


    void SaveHeightMap(const std::string& filename);
    void SaveSplatMap(const std::string& filename);
    void SaveAoMap(const std::string& filename);

    void AddToPhysicsWorld(PhysicsWorld* physicsWorld);

    void SetCursorSize(int cursorSize);

    AABB GetAABB()const;

    void CreateAoMap(const std::string& aoMapFilename, bool guiMode);
    void BakeAo(int samples=32, int waveLength=300, int amplitude=200, float distAttenuation = 1.7);

    Vector3f ComputeHeightMapPos(int x, int z);
    float ComputeHeightMapHeight(int x, int z);
    Vector3f ComputeHeightMapNormal(int x, int z);

    void SetGuiMode(int guiMode);

    void AddControlPoint();

    void BuildRoad();
    void DeleteCP();

    std::vector<std::string> GetDefaultDefines()const;

    int GetResolution()const {
	return m_resolution;
    }

    Texture2D* GetHeightMap() {
        return m_heightMap.get();
    }

    Vector2i GetCursorPosition()const {
        return m_cursorPosition;
    }

    float GetYScale()const {
	return m_yScale;
    }

    float GetXzScale()const {
	return m_xzScale;
    }


    Vector3f GetOffset()const {
	return m_offset;
    }

    Vector2f ToLocalPos(Vector3f pos)const;
};
