#pragma once

#include "ewa/gl/cube_map_texture.hpp"

class ShaderProgram;
class VBO;
class ICamera;
class Paraboloid;

/*
  Responsible for skybox rendering.
 */
class Skybox {

private:

    VBO* m_indexBuffer;
    VBO* m_positionBuffer;

    ShaderProgram* m_deferredShader;
    ShaderProgram* m_forwardShader;
    ShaderProgram* m_envMapShader;

    GLushort m_numIndices;

public:

    Skybox();
    ~Skybox();

    // render skybox for deferred rendering.
    void DrawDeferred(CubeMapTexture* m_cubeMap, const ICamera* camera, Texture* depthMap, int windowWidth, int windowHeight);

    // render skybox for forward rendering.
    void DrawForward(CubeMapTexture* m_cubeMap, const ICamera* camera);

    // render to environment map.
    void DrawEnvMap(CubeMapTexture* m_cubeMap, Paraboloid& paraboloid);
};
