#pragma once

#include "ewa/application.hpp"
#include "ewa/common.hpp"

#include "ewa/math/vector4f.hpp"
#include "ewa/math/matrix4f.hpp"

#include <LinearMath/btQuaternion.h>

#include "gui_listener.hpp"
#include "geometry_object_listener.hpp"
#include "notifier.hpp"


#include <map>

class ICamera;
class HeightMap;
class Skydome;
class Grass;
class Sound;
class ParticleSystem;
class IGeometryObject;
class DepthFBO;
class ViewFrustum;
class PhysicsWorld;
class Car;
class CarCamera;
class Gui;
class PickingFBO;
class Gbuffer;
class GpuProfiler;
class Line;
class Cube;
class SsaoPass;
class LightingPass;
class Skybox;
class CubeMapTexture;
class EnvFBO;
class Grid;
class ColorFBO;
class ColorDepthFbo;
class ParticlesRenderer;
class DualParaboloidMap;
class Camera;
class ShaderProgram;

constexpr int DEPTH_FBO_TEXTURE_UNIT = 9;
constexpr int PICKING_FBO_TEXTURE_UNIT = 10;
constexpr int ENV_FBO_TEXTURE_UNIT = 11;
constexpr int REFRACTION_FBO_TEXTURE_UNIT = 12;
constexpr int REFLECTION_FBO_TEXTURE_UNIT = 13;

/*
  This is our main application

  It contains the main rendering logic.
 */
class TuhuApplication : public Application, public GuiListener, public GeometryObjectListener, public Notifier{

private:

    std::unique_ptr<Camera> m_freeCamera;
    std::unique_ptr<ICamera> m_carCamera;
    Camera* m_curCamera; // current camera.

    std::unique_ptr<ICamera> m_reflectionCamera; // camera for reflection rendering.
    std::unique_ptr<HeightMap> m_heightMap;
    std::unique_ptr<Skydome> m_skydome;
    std::unique_ptr<Grass> m_grass;

    std::unique_ptr<Sound> m_windSound;
    std::unique_ptr<ParticlesRenderer> m_particlesRenderer;

    IGeometryObject* m_selected; // selected object in gui editor.

    std::unique_ptr<EnvFBO> m_envFbo;
    std::unique_ptr<ColorDepthFbo> m_refractionFbo; // for refraction rendering.
    std::unique_ptr<ColorFBO> m_reflectionFbo; // for reflection rendering.
    std::unique_ptr<ColorFBO> m_colorFbo;

    std::unique_ptr<Car> m_car; // car object.

    std::unique_ptr<Line> m_line;
    std::unique_ptr<Gui> m_gui;

    std::unique_ptr<DepthFBO> m_depthFbo; // for shadow map rendering.
    std::unique_ptr<PickingFBO> m_pickingFbo;// picking buffer.
    std::unique_ptr<Gbuffer> m_gbuffer; // for deferred rendering.

    std::unique_ptr<Grid> m_grid;

    Vector4f m_lightDirection; // light direction, of our single directional light source.

    // used in shadow mapping for rendering all object from the perspective of the light source.
    Matrix4f m_lightVp;

    float m_totalDelta;

    std::unique_ptr<ViewFrustum> m_cameraFrustum; // frustum used for view frusutm culling.
    std::unique_ptr<ViewFrustum> m_lightFrustum;
    std::unique_ptr<ViewFrustum> m_reflectionFrustum; // used for culling when drawing reflections.

    std::unique_ptr<DualParaboloidMap> m_dualParaboloidMap; // for drawing reflections on the car.
    CubeMapTexture* m_cubeMapTexture;

    std::unique_ptr<PhysicsWorld> m_physicsWorld;

    unsigned int currentObjId;

    std::unique_ptr<Cube> m_aabbWireframe;

    std::unique_ptr<Skybox> m_skybox;


    Vector3f m_prevCameraPos; // camera pos for previous frame.

    ShaderProgram* m_fxaaShader;

    float m_doAA;

    std::map<unsigned int,IGeometryObject*> m_geoObjs;

    IGeometryObject* LoadObj(const std::string& path, const Vector3f& position,
			     const btQuaternion& rotation = btQuaternion::getIdentity(), float scale = 1.0f);

    Matrix4f MakeLightProj(int frameBufferWidth, int frameBufferHeight)const;

    void StartPhysics();

    void ParseObjs(const std::string& filename);

    std::unique_ptr<GpuProfiler> m_gpuProfiler;

    std::unique_ptr<SsaoPass> m_ssaoPass;
    std::unique_ptr<LightingPass> m_lightingPass;

    bool mOptAoOnly = false;
    bool mOptEnableAo = true;

    int mNonCulledObjects = 0;
    int mTotalObjects = 0;


    void UpdateMatrices();
public:

    TuhuApplication(int argc, char *argv[]);
    ~TuhuApplication();

    void Init() override;
    void Render() override;
    void RenderText() override;
    void Update(const float delta) override;
    void Cleanup() override;

    void RenderShadowMap();
    void RenderEnvMap();
    void RenderRefraction();
    void RenderReflection();

    void RenderScene();
    void RenderId();

    void RenderParticles();

    /*
      Implementation of GuiListener
     */
    virtual void TranslationAccepted();
    virtual void RotationAccepted();
    virtual void ScaleAccepted();

    virtual void ModelAdded(const std::string& filename);
    virtual void CursorSizeChanged();
    virtual void Duplicate();
    virtual void Delete();

    virtual void BakeAo(int samples, int waveLength, int amplitude, float distAttenuation);

    virtual void LightUpdate();

    virtual void BuildRoad();
    virtual void DeleteCP();

    virtual void GrassPuff() {}
};
