#pragma once

#include "particle_system.hpp"

// fire effect rendered with a particle system.
class FireEffect : public ParticleSystem {

public:
    FireEffect(const Vector3f& pos);

    virtual ~FireEffect() {}
};
