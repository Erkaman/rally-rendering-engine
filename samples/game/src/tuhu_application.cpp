#include "tuhu_application.hpp"

#include "particles_renderer.hpp"
#include "dual_paraboloid_map.hpp"
#include "math/vector4f.hpp"

#include "ewa/camera.hpp"
#include "ewa/common.hpp"
#include "ewa/font.hpp"
#include "ewa/keyboard_state.hpp"
#include "ewa/mouse_state.hpp"
#include "ewa/file.hpp"
#include "ewa/buffered_file_reader.hpp"
#include "ewa/string_util.hpp"
#include "ewa/resource_manager.hpp"
#include "ewa/timer.hpp"

#include "ewa/gl/depth_fbo.hpp"
#include "ewa/gl/texture.hpp"
#include "ewa/gl/cube_map_texture.hpp"

#include "ewa/audio/sound.hpp"
#include "ewa/audio/wave_loader.hpp"

#include "skydome.hpp"
#include "height_map.hpp"
#include "grass.hpp"
#include "particle_system.hpp"
#include "smoke_effect.hpp"
#include "fire_effect.hpp"
#include "ssao_pass.hpp"
#include "lighting_pass.hpp"

#include "ewa/line.hpp"
#include "ewa/points.hpp"
#include "ewa/cube.hpp"
#include "ewa/view_frustum.hpp"

#include "ewa/config.hpp"

#include "ewa/physics_world.hpp"
#include "car_camera.hpp"

#include "picking_fbo.hpp"
#include "ewa/gl/color_depth_fbo.hpp"

#include "ewa/gl/color_fbo.hpp"


#include "car.hpp"
#include "gui_enum.hpp"
#include "bt_util.hpp"

#include "gui.hpp"
#include "gui_mouse_state.hpp"

#include "gpu_profiler.hpp"
#include "physics_mask.hpp"
#include "gbuffer.hpp"

#include "skybox.hpp"
#include "env_camera.hpp"
#include "env_fbo.hpp"
#include "grid.hpp"

using namespace std;


constexpr int SHADOW_MAP_SIZE =
    HighQuality ?
    1024*2*2 :
    1024*2;

constexpr int WINDOW_WIDTH = HighQuality ? 1600 : 1200;
constexpr int WINDOW_HEIGHT = HighQuality ?  960 : 720;

constexpr int REFRACTION_WIDTH = WINDOW_WIDTH / 1;
constexpr int REFRACTION_HEIGHT = WINDOW_HEIGHT / 1;

constexpr int REFLECTION_WIDTH = WINDOW_WIDTH / 2;
constexpr int REFLECTION_HEIGHT = WINDOW_HEIGHT / 2;

const string HEIGHT_MAP_FILENAME = "heightmap.bin";
const string SPLAT_MAP_FILENAME = "splatmap.bin";
const string AO_MAP_FILENAME = "aomap.bin";

const string OBJS_FILENAME = "objs";
const string GRASS_FILENAME = "grass";

bool save_screenshot(string filename, int w, int h)
{
    w *= 2;
    h *= 2;

    //This prevents the images getting padded
    // when the width multiplied by 3 is not a multiple of 4
    glPixelStorei(GL_PACK_ALIGNMENT, 1);

    int nSize = w*h*3;
    // First let's create our buffer, 3 channels per Pixel
    char* dataBuffer = (char*)malloc(nSize*sizeof(char));

    if (!dataBuffer) return false;

    // Let's fetch them from the backbuffer
    // We request the pixels in GL_BGR format, thanks to Berzeger for the tip
    glReadPixels((GLint)0, (GLint)0,
                 (GLint)w, (GLint)h,
		 GL_BGR, GL_UNSIGNED_BYTE, dataBuffer);

    //Now the file creation
    FILE *filePtr = fopen(filename.c_str(), "wb");
    if (!filePtr) return false;

    unsigned char TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};
    unsigned char header[6] = {(unsigned char) (w%256), (unsigned char) (w/256),
                               (unsigned char) (h%256), (unsigned char)( h/256),
			       24,0};
    // We write the headers
    fwrite(TGAheader,	sizeof(unsigned char),	12,	filePtr);
    fwrite(header,	sizeof(unsigned char),	6,	filePtr);
    // And finally our image data
    fwrite(dataBuffer,	sizeof(GLubyte),	nSize,	filePtr);
    fclose(filePtr);

    return true;
}

void ToClipboard(const std::string& str) {
    std::string command = "echo '" + str + "' | pbcopy";
    system(command.c_str());
}

TuhuApplication::TuhuApplication(int argc, char *argv[]):Application(argc, argv, WINDOW_WIDTH, WINDOW_HEIGHT),m_lightDirection (
    Vector3f(-0.705072f, -0.958142f, -0.705072f).Normalize(),
    0.0f)   { }

TuhuApplication::~TuhuApplication() {
}

void TuhuApplication::Init() {

    /*
      In here, we initialize all the objects of the applicaiton.
    */

    m_doAA = 1.0;

    vector<string> defines;

    string shaderName = "shader/fxaa";
    m_fxaaShader =
	ResourceManager::LoadShader(shaderName + "_vs.glsl", shaderName + "_fs.glsl", defines);

    m_aabbWireframe.reset(Cube::Load());

    m_gpuProfiler.reset(new GpuProfiler());
    m_ssaoPass.reset(new SsaoPass(GetFramebufferWidth(),GetFramebufferHeight()));
    m_lightingPass.reset(new LightingPass(GetFramebufferWidth(),GetFramebufferHeight()));

    m_grid.reset(new Grid());

    m_skybox.reset(new Skybox());

    m_envFbo.reset(new EnvFBO());
    m_envFbo->Init(ENV_FBO_TEXTURE_UNIT, 512, 512);

    m_refractionFbo.reset(new ColorDepthFbo());
    m_refractionFbo->Init(REFRACTION_FBO_TEXTURE_UNIT, REFRACTION_WIDTH, REFRACTION_HEIGHT);

    m_reflectionFbo.reset(new ColorFBO());
    m_reflectionFbo->Init(REFLECTION_FBO_TEXTURE_UNIT, REFLECTION_WIDTH, REFLECTION_HEIGHT);

    m_colorFbo.reset(new ColorFBO());
    m_colorFbo->Init(0, GetFramebufferWidth(),GetFramebufferHeight());

    m_dualParaboloidMap.reset(new DualParaboloidMap());

    m_cubeMapTexture = CubeMapTexture::Load(
	"img/daylight_ft.png",
	"img/daylight_bk.png",
	"img/daylight_lf.png",
	"img/daylight_rt.png",
	"img/daylight_up.png",
	"img/daylight_dn.png"
	);
    if(m_cubeMapTexture == NULL) {
	PrintErrorExit();
    }

    m_cameraFrustum.reset(new ViewFrustum());
    m_lightFrustum.reset(new ViewFrustum());
    m_reflectionFrustum.reset(new ViewFrustum());
    m_skydome.reset(new Skydome(1, 10, 10));

    m_particlesRenderer.reset(new ParticlesRenderer(GetFramebufferWidth(), GetFramebufferHeight()));
    const Vector3f pos = Vector3f(-9.497000, 12.197082, -2.078500);
    m_freeCamera.reset(new Camera(
                           GetFramebufferWidth(),
                           GetFramebufferHeight(),
                           pos,
                           Vector3f(0.871404, -0.483328, -0.083955)
                           ));

    if(m_gui) {
	m_heightMap->SetCursorSize(m_gui->GetCursorSize());

	// do a first update, in order to setup the PBOs
	m_heightMap->UpdateGui(0, NULL, 0,0);
	m_heightMap->UpdateGui(0, NULL, 0,0);
    }

    if(m_gui) {
	m_pickingFbo.reset(new PickingFBO());
	m_pickingFbo->Init(PICKING_FBO_TEXTURE_UNIT, GetFramebufferWidth(),GetFramebufferHeight() );
    }
    m_gbuffer.reset(new Gbuffer());
    // TODO: should not this be the size of the framebuffer?
    m_gbuffer->Init(0, GetFramebufferWidth(),GetFramebufferHeight() );

    m_depthFbo.reset(new DepthFBO());

    m_depthFbo->Init(DEPTH_FBO_TEXTURE_UNIT, SHADOW_MAP_SIZE, SHADOW_MAP_SIZE);

    string dir = Config::GetInstance().GetWorldFilename();

    Vector3f trans = Vector3f(-2,9.0,-2);
    m_car.reset(new Car());
    bool result = m_car->Init(Vector3f(0,-1.5,0)+trans);

    if(!result) {
	LOG_I("could not load car");
	PrintErrorExit();
    }

    m_geoObjs[m_car->GetId()] = m_car.get();

    bool guiMode = (m_gui != 0);

    m_line.reset(Line::Load(Vector3f(0), -1000.0f * Vector3f(m_lightDirection), Vector3f(1,0,0) ));

    if(ResourceManager::GetInstance().PathExists(dir)) { // load scene specified in file.
	m_heightMap.reset(new HeightMap(
                              File::AppendPaths(dir, HEIGHT_MAP_FILENAME ) ,
                              File::AppendPaths(dir, SPLAT_MAP_FILENAME ),
                              File::AppendPaths(dir, AO_MAP_FILENAME ),

                              guiMode));

	ParseObjs(File::AppendPaths(dir, OBJS_FILENAME ));

	m_grass.reset(new Grass(File::AppendPaths(dir, GRASS_FILENAME ), m_heightMap.get() ));
    } else {  // load default scene.
	m_heightMap.reset(new HeightMap(guiMode));

	LoadObj("obj/wood_floor.eob", Vector3f(-10,0,40)+ trans );

	LoadObj("obj/plane.eob", Vector3f(0,-2.5,0)+ trans);

	m_selected = NULL;

	m_grass.reset(new Grass(m_heightMap.get() ));
    }
    m_physicsWorld.reset(new PhysicsWorld(m_heightMap->GetAABB() ));

    m_carCamera.reset(new CarCamera(GetFramebufferWidth(),GetFramebufferHeight(),
                                    m_car.get()));
    m_curCamera = m_freeCamera.get();

    LightUpdate();

    currentObjId = 0;
    m_totalDelta = 0;

    Config& m_config = Config::GetInstance();
    if(m_config.IsGui()) {

	m_gui.reset(new Gui(m_window));
	m_gui->AddListener(this);

	GuiMouseState::Init(m_guiVerticalScale);

    } else {
	LOG_I("No GUI created");
    }

    ::SetDepthTest(true);
    ::SetCullFace(true);
}

// make lighting projection matrix.
Matrix4f TuhuApplication::MakeLightProj(int frameBufferWidth, int frameBufferHeight)const {

    /*
      First we compute the world space location of all 8 corners of the view frustum.
    */

    Matrix4f invProj = m_curCamera->GetVp().Inverse();

    const float zNear =  -1.0f;
    const float zFar  = 1.0f;

    // left bottom far
    Vector4f temp = Vector4f(-1,-1,zFar,1.0f);
    const Vector3f lbf = m_cameraFrustum->lbf();

    // left top far
    const Vector3f ltf = m_cameraFrustum->ltf();

    // right bottom far
    const Vector3f rbf = m_cameraFrustum->rbf();

    // right top far
    const Vector3f rtf = m_cameraFrustum->rtf();

    // left bottom near
    const Vector3f lbn = m_cameraFrustum->lbn();

    // left top near
    const Vector3f ltn = m_cameraFrustum->ltn();


    // right bottom far
    const Vector3f rbn = m_cameraFrustum->rbn();

    // right top far
    const Vector3f rtn = m_cameraFrustum->rtn();

    Vector3f corners[8] =  {lbn , ltn , rbn , rtn ,
			    lbf , ltf , rbf , rtf };

    Vector3f centroid = Vector3f(0,0,0);
    for(int i = 0; i < 8; ++i) {
	centroid += corners[i];
    }
    centroid = centroid * (1.0f/8.0f);


    Config& config = Config::GetInstance();

    const float nearClipOffset = 70.0f;


    Matrix4f viewMatrix =
	Matrix4f::CreateLookAt(
	    centroid - ( Vector3f(m_lightDirection) * (config.GetZFar() + nearClipOffset )   ),
	    centroid,
	    Vector3f(0,1,0));

    // the frustum corners in lightspace.
    Vector3f cornersLS[8];

    for(int i = 0; i < 8; ++i) {
	cornersLS[i] = Vector3f(viewMatrix * Vector4f(corners[i], 1.0));
    }


    Vector3f mins = cornersLS[0];

    Vector3f maxes = cornersLS[0];

    for (int i = 0; i < 8; i++){

	if (cornersLS[i].x > maxes.x)
	    maxes.x = cornersLS[i].x;
	else if (cornersLS[i].x < mins.x)
	    mins.x = cornersLS[i].x;

	if (cornersLS[i].y > maxes.y)
	    maxes.y = cornersLS[i].y;
	else if (cornersLS[i].y < mins.y)
	    mins.y = cornersLS[i].y;

	if (cornersLS[i].z > maxes.z)
	    maxes.z = cornersLS[i].z;
	else if (cornersLS[i].z < mins.z)
	    mins.z = cornersLS[i].z;
    }

    return Matrix4f::CreateOrthographic(mins.x, maxes.x,
					mins.y,
					maxes.y,
					-maxes.z - nearClipOffset,
					-mins.z ) * viewMatrix;
}

void TuhuApplication::RenderShadowMap() {

    m_depthFbo->Bind();
    {
	::SetViewport(0,0,SHADOW_MAP_SIZE,SHADOW_MAP_SIZE);

	Clear(0.0f, 1.0f, 1.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GeometryObject::RenderShadowMapAll(m_lightVp);

	m_heightMap->RenderShadowMap(m_lightVp);
    }
    m_depthFbo->Unbind();
}

void TuhuApplication::RenderId() {
    m_pickingFbo->Bind();

    Clear(0.0f, 0.0f, 0.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GeometryObject::RenderIdAll(m_curCamera);

    m_grass->RenderIdAll(m_curCamera);

    m_pickingFbo->Unbind();
}

void TuhuApplication::RenderScene() {
    Matrix4f biasMatrix(
	0.5f, 0.0f, 0.0f, 0.5f,
	0.0f, 0.5f, 0.0f, 0.5f,
	0.0f, 0.0f, 0.5f, 0.5f,
	0.0f, 0.0f, 0.0f, 1.0f
	);

    Matrix4f lightVp =  biasMatrix*   m_lightVp;

    m_gpuProfiler->Begin(GTS_Objects);
    {
	GeometryObject::RenderAll(m_curCamera, m_lightDirection, lightVp, *m_depthFbo, m_envFbo->GetEnvMap(), *m_refractionFbo, *m_reflectionFbo);
    }

    m_gpuProfiler->End(GTS_Objects);

    m_gpuProfiler->Begin(GTS_Terrain);

    bool aoOnly = m_gui ? m_gui->isAoOnly() : false;

    m_heightMap->Render(m_curCamera, m_lightDirection, lightVp, *m_depthFbo, aoOnly);

    m_gpuProfiler->End(GTS_Terrain);

    m_gpuProfiler->Begin(GTS_Grass);

    m_grass->DrawDeferred(m_curCamera, m_lightDirection);
    m_gpuProfiler->End(GTS_Grass);
}

void TuhuApplication::RenderEnvMap() {

    size_t size = m_dualParaboloidMap->GetSize();

    for(int i = 0 ; i < 2; ++i) {

	Paraboloid par = m_dualParaboloidMap->GetParaboloid(i);

        par.m_fbo->Bind();

	::SetViewport(0,0,size,size);
	Clear(0.0f, 0.0f, 0.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

       	m_skybox->DrawEnvMap(m_cubeMapTexture, par);

	GeometryObject::RenderAllEnv(m_lightDirection, i, par);

	m_grass->DrawEnvMap(&par, m_lightDirection, i);

	bool aoOnly = m_gui ? m_gui->isAoOnly() : false;
	m_heightMap->RenderParaboloid(par, m_lightDirection, aoOnly);


	par.m_fbo->Unbind();
    }
}

void TuhuApplication::RenderRefraction() {

    m_refractionFbo->Bind();
    {
	::SetViewport(0,0,REFRACTION_WIDTH, REFRACTION_HEIGHT);
	Clear(0.0f, 1.0f, 1.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	bool aoOnly = m_gui ? m_gui->isAoOnly() : false;
	m_heightMap->RenderRefraction(m_curCamera, m_lightDirection, aoOnly);
    }
    m_refractionFbo->Unbind();
}

void TuhuApplication::RenderReflection() {

    m_reflectionFbo->Bind();
    {
	::SetViewport(0,0,REFLECTION_WIDTH, REFLECTION_HEIGHT);
	Clear(0.0f, 1.0f, 1.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_skybox->DrawForward(m_cubeMapTexture, m_reflectionCamera.get());

	GeometryObject::RenderReflection(m_reflectionCamera.get(), m_lightDirection);

	m_grass->DrawReflection(m_reflectionCamera.get(), m_lightDirection);

	bool aoOnly = m_gui ? m_gui->isAoOnly() : false;
	m_heightMap->RenderReflection(m_reflectionCamera.get(), m_lightDirection, aoOnly);

    }
    m_reflectionFbo->Unbind();
}

// do all rendering logic.
void TuhuApplication::Render() {
    if(m_gui) {
	m_gui->NewFrame(m_guiVerticalScale);
    }

    m_gpuProfiler->Begin(GTS_Shadows);
    RenderShadowMap();
    m_gpuProfiler->End(GTS_Shadows);

    m_gpuProfiler->Begin(GTS_EnvMap);
    RenderEnvMap();
    m_gpuProfiler->End(GTS_EnvMap);

    m_gpuProfiler->Begin(GTS_Refraction);
    RenderRefraction();
    m_gpuProfiler->End(GTS_Refraction);

    m_gpuProfiler->Begin(GTS_Reflection);
    RenderReflection();
    m_gpuProfiler->End(GTS_Reflection);

    float SCALE = m_guiVerticalScale;

    int windowWidth;
    int windowHeight;

    SetViewport();
    Clear(0.0f, 1.0f, 1.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(m_pickingFbo)
	RenderId();

    // start writing to gbuffer.
    m_gbuffer->BindForWriting();

    SetViewport();
    Clear(0.0f, 1.0f, 1.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    RenderScene();

    m_gbuffer->UnbindForWriting();

    Matrix4f biasMatrix(
	0.5f, 0.0f, 0.0f, 0.5f,
	0.0f, 0.5f, 0.0f, 0.5f,
	0.0f, 0.0f, 0.5f, 0.5f,
	0.0f, 0.0f, 0.0f, 1.0f
	);

    Matrix4f lightVp =  biasMatrix*   m_lightVp;

    bool aoOnly = m_gui ? m_gui->isAoOnly() : mOptAoOnly;
    bool enableAo = m_gui ? m_gui->IsEnableAo() : mOptEnableAo;

    m_colorFbo->Bind();
    {
	SetViewport();
	Clear(0.0f, 1.0f, 1.0f, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_gpuProfiler->Begin(GTS_Light);
	m_lightingPass->Render(
	    m_gbuffer.get(), m_curCamera, m_lightDirection,
	    lightVp, *m_depthFbo, GeometryObject::GetTorches(),
	    *m_dualParaboloidMap, *m_refractionFbo, *m_reflectionFbo,
	    *m_cameraFrustum,
	    aoOnly,
	    enableAo
	    );
	m_gpuProfiler->End(GTS_Light);

    }
    m_colorFbo->Unbind();

    m_gpuProfiler->Begin(GTS_FXAA);
    {
        m_fxaaShader->Bind();

        m_fxaaShader->SetUniform("colorTexture",0 );
        Texture::SetActiveTextureUnit(0);
        m_colorFbo->GetRenderTargetTexture().Bind();

        m_fxaaShader->SetUniform("doAA",m_doAA );


        GL_C(glDrawArrays(GL_TRIANGLES, 0, 3));

        m_colorFbo->GetRenderTargetTexture().Unbind();
        m_fxaaShader->Unbind();

    }
    m_gpuProfiler->End(GTS_FXAA);

    m_gpuProfiler->Begin(GTS_Sky);
    {
        m_skybox->DrawDeferred(
            m_cubeMapTexture,
            m_curCamera, m_gbuffer->GetDepthTexture(), GetFramebufferWidth(), GetFramebufferHeight() );
    }
    m_gpuProfiler->End(GTS_Sky);

    m_gpuProfiler->Begin(GTS_Particles);
    {
        m_particlesRenderer->Render(m_gbuffer.get(), m_curCamera, GetFramebufferWidth(), GetFramebufferHeight() );
    }
    m_gpuProfiler->End(GTS_Particles);

    if(m_gui) {

	int fb_width, fb_height;
	fb_width = GetFramebufferWidth();
	fb_height = GetFramebufferHeight();

 	windowWidth = fb_width*SCALE * 0.5f;
	windowHeight = fb_height * 0.5f;

	m_gui->Render(windowWidth, windowHeight);
    }

    m_gpuProfiler->WaitForDataAndUpdate();

    m_gpuProfiler->EndFrame();

}

// handle all update logic in here. and handle all keyboard and mouse input.
void TuhuApplication::Update(const float delta) {
    MouseState& ms = MouseState::GetInstance();

    m_totalDelta += delta;

    m_curCamera->Update(delta);
    m_reflectionCamera.reset(m_curCamera->CreateReflectionCamera());

    GeometryObject::SetTotalDelta(m_totalDelta);


    m_particlesRenderer->Update(m_cameraFrustum.get(), m_curCamera, delta);

    if(m_gui)
	GuiMouseState::Update(GetFramebufferWidth(), GetFramebufferHeight());

    m_cameraFrustum->Update( m_curCamera->GetVp(), m_curCamera->GetPosition() );
    m_reflectionFrustum->Update( m_reflectionCamera->GetVp(),  m_curCamera->GetPosition()  );

    UpdateMatrices();

    Vector3f curCameraPos = m_curCamera->GetPosition();
    Vector3f cameraDirection = ((curCameraPos - m_prevCameraPos) * (1.0 / delta) ).Normalize();

    m_grass->Update(delta,  m_heightMap->ToLocalPos(m_curCamera->GetPosition()), cameraDirection,
		    *m_cameraFrustum, *m_lightFrustum, *m_dualParaboloidMap, *m_reflectionFrustum
        );

    m_prevCameraPos = curCameraPos;

    m_lightFrustum->Update( m_lightVp, Vector3f(0) );

    m_physicsWorld->Update(delta);

    for(auto& it : m_geoObjs) {
	IGeometryObject* geoObj = it.second;
	geoObj->Update(m_cameraFrustum.get(), m_lightFrustum.get(), *m_dualParaboloidMap, m_reflectionFrustum.get() );
    }

    m_heightMap->Update(*m_cameraFrustum, *m_lightFrustum, m_car->GetLightFrustums(), *m_reflectionFrustum, *m_dualParaboloidMap);

    if(m_gui) {
	m_gui->Update();
	m_heightMap->SetGuiMode( m_gui->GetGuiMode() );
    }

    m_heightMap->UpdateGui(delta, m_curCamera, (float)GetFramebufferWidth(),(float)GetFramebufferHeight());
    m_dualParaboloidMap->Update( m_car->GetPosition() );

    /*
      next, handle keyboard input.
     */

    KeyboardState& kbs = KeyboardState::GetInstance();

    if( kbs.IsPressed(GLFW_KEY_P) ) {

	string out = "Vector3f" +tos(m_curCamera->GetPosition()) + ",";
	out += "Vector3f" + tos(m_curCamera->GetViewDir());
	ToClipboard(out);
    }

    if( kbs.WasPressed(GLFW_KEY_6)
        //&& !m_gui
        ) {
	mOptAoOnly = !mOptAoOnly;
    }
    if( kbs.WasPressed(GLFW_KEY_5)
        //&& !m_gui
        ) {
	mOptEnableAo = !mOptEnableAo;
    }


    if( kbs.WasPressed(GLFW_KEY_8) ) {
	m_grass->BlowWind();
    }


    if( kbs.WasPressed(GLFW_KEY_7) ) {

	if(m_doAA > 0.0) {
	    m_doAA = -1.0;
	} else {

	    m_doAA = +1.0;
	}

	LOG_I("aa: %f", m_doAA );


    }

    if(GuiMouseState::isWithinWindow()) {

	if(m_gui->GetGuiMode() == ModifyTerrainMode) {

	    if(ms.IsPressed(GLFW_MOUSE_BUTTON_1 )) {

		if(m_gui->GetTerrainMode() == ModifyElevationMode)
		    m_heightMap->ModifyTerrain(delta, +m_gui->GetStrength()  );
		else if(m_gui->GetTerrainMode() == DistortMode){
		    m_heightMap->DistortTerrain(delta, +m_gui->GetStrength(), m_gui->GetNoiseScale()  );
		} else if(m_gui->GetTerrainMode() == SmoothMode) {

		    m_heightMap->SmoothTerrain(delta, m_gui->GetSmoothRadius() );
		} else {
		    // flat.
		    m_heightMap->LevelTerrain(delta , m_gui->GetStrength());

		}
	    }

	    if(ms.IsPressed(GLFW_MOUSE_BUTTON_2 )) {

		if(m_gui->GetTerrainMode() == ModifyElevationMode)
		    m_heightMap->ModifyTerrain(delta, -m_gui->GetStrength() );
	    }

	} else if(m_gui->GetGuiMode() == DrawTextureMode) {


	    if(ms.IsPressed(GLFW_MOUSE_BUTTON_1 )) {
		m_heightMap->DrawTexture(delta, m_gui->GetDrawTextureType() );
	    }
	} else if(m_gui->GetGuiMode() == RoadMode) {

	    if(ms.WasPressed(GLFW_MOUSE_BUTTON_1 )) {
		m_heightMap->AddControlPoint();
	    }
	}



    }

    if(ms.WasPressed(GLFW_MOUSE_BUTTON_1 ) && GuiMouseState::isWithinWindow()) {

	if(m_gui->GetGuiMode() == ModelMode) {

	    float y = GetFramebufferHeight() - GuiMouseState::GetY() - 1;
	    float x = GuiMouseState::GetX();

	    PixelInfo pi = m_pickingFbo->ReadPixel((unsigned int)x,(unsigned int)y);

	    unsigned int id = (unsigned int)pi.id;

	    if(id != 0) {

		// id of 0 is no object picked at all
		// however, the ids in the map are 0-based.
		// thus decrease by one.
		id -= 1;

		if(m_selected){
		    // deselected formerly selected object.
		    m_selected->SetSelected(false);
		}

		LOG_I("selected obj: %d", id);

		m_selected = m_geoObjs[id];
		m_selected->SetSelected(true);
	    }
	}
    }

    if(m_gui && m_gui->GetGuiMode() == GrassMode && GuiMouseState::isWithinWindow()) {

	if(ms.WasPressed(GLFW_MOUSE_BUTTON_1 ) ) {

//	    LOG_I("add grass at :%s", string(m_heightMap->GetCursorPosition() ).c_str() );
	    m_grass->AddGrass(m_heightMap->GetCursorPosition(), m_gui->GetGrassClusterSize());
	}

	if(ms.IsPressed(GLFW_MOUSE_BUTTON_2 ) ) {

	    float y = GetFramebufferHeight() - GuiMouseState::GetY() - 1;
	    float x = GuiMouseState::GetX();

	    PixelInfo pi = m_pickingFbo->ReadPixel((unsigned int)x,(unsigned int)y);

	    unsigned int id = (unsigned int)pi.unused2;

	    if(id != 0) {

		// because ids are zero-based(but the ids in the FBO are one-based)
		--id;

//		LOG_I("REMOVE: %d", id);

                m_grass->RemoveGrass(id);

	    }

	}

    }

    if(m_gui && m_gui->GetGuiMode() == ModelMode && m_selected ) {
	m_selected->SetEditPosition( m_gui->GetTranslation() );
	m_selected->SetEditRotation( toBtQuat(m_gui->GetRotation()) );
	m_selected->SetEditScale( m_gui->GetScale());
    }

    static bool b= false;

    if( kbs.IsPressed(GLFW_KEY_B) && !b ) {
	LOG_I("play sound");
	m_windSound->Play();

	b = true;
    }

    if( kbs.WasPressed(GLFW_KEY_N) ) {
	m_curCamera->PrintState();
    }



}

string Format(const string& fmt, float val) {

    char buffer[30];

    sprintf(buffer,
	    fmt.c_str(),
	    val);

    return string(buffer);
}

void TuhuApplication::RenderText()  {

    string cull = std::to_string(mNonCulledObjects) + "\\" + std::to_string(mTotalObjects);

    m_font->DrawString(*m_fontShader, 750,170, cull);

    m_font->DrawString(*m_fontShader, 750,120, tos(m_curCamera->GetPosition())  );

    m_font->DrawString(*m_fontShader, 750,220,
		       Format("Objects: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Objects) ) );

    m_font->DrawString(*m_fontShader, 750,270,
		       Format("Sky: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Sky) ) );

    m_font->DrawString(*m_fontShader, 750,320,
		       Format("Terrain: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Terrain) ) );

    m_font->DrawString(*m_fontShader, 750,370,
		       Format("Shadows: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Shadows) ) );

    m_font->DrawString(*m_fontShader, 750,430,
		       Format("Light<x: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Light) ) );

    m_font->DrawString(*m_fontShader, 750,490,
		       Format("EnvMap: %0.2f ms", m_gpuProfiler->DtAvg(GTS_EnvMap) ) );

    m_font->DrawString(*m_fontShader, 750,550,
		       Format("Refraction: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Refraction)) );

    m_font->DrawString(*m_fontShader, 750,610,
		       Format("Reflection: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Reflection)) );

    m_font->DrawString(*m_fontShader, 750,690,
		       Format("Particles: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Particles)) );

    m_font->DrawString(*m_fontShader, 750,750,
		       Format("Grass: %0.2f ms", m_gpuProfiler->DtAvg(GTS_Grass)) );

    m_font->DrawString(*m_fontShader, 750,810,
		       Format("FXAA: %0.2f ms", m_gpuProfiler->DtAvg(GTS_FXAA)) );


/*    m_font->DrawString(*m_fontShader, 750,810,
      Format("AA: %f", (m_doAA > 0.0f) ? 1.0f : 0.0f  ));
*/


}

IGeometryObject* TuhuApplication::LoadObj(const std::string& path, const Vector3f& position,
					  const btQuaternion& rotation, float scale) {


    GeometryObject* obj = new GeometryObject();

//    LOG_I("add model id: %d", currentObjId);
    bool result = obj->Init(path, position,rotation, scale, currentObjId++, COL_STATIC, staticCollidesWith);

    if(!result)
	PrintErrorExit();

    m_geoObjs[obj->GetId()] = obj;

    return obj;
}

void TuhuApplication::StartPhysics()  {

    static bool once = false;

    if(once)
	return;

    for(auto& it : m_geoObjs) {
	IGeometryObject* geoObj = it.second;

	if(geoObj == m_car.get() || geoObj->GetFilename() == string("obj/plane.eob"))
	    geoObj->AddToPhysicsWorld(m_physicsWorld.get());
    }
    m_heightMap->AddToPhysicsWorld(m_physicsWorld.get());

    once = true;

}

void TuhuApplication::Cleanup() {

    string dir = Config::GetInstance().GetWorldFilename();

    if(m_gui) { // if we were in the world editor, we need to serialize the world.

	m_grass->SaveGrass(File::AppendPaths(dir, GRASS_FILENAME ) );

	// we save the entire world in a directory. Make sure the directory exists:
	File::CreatePath(dir);

	// save heightmap:
	m_heightMap->SaveHeightMap(File::AppendPaths(dir, HEIGHT_MAP_FILENAME ) );

	// save splatmap:
	m_heightMap->SaveSplatMap(File::AppendPaths(dir, SPLAT_MAP_FILENAME ) );

        // save ao map.
	m_heightMap->SaveAoMap(File::AppendPaths(dir, AO_MAP_FILENAME ) );

	File* outFile = File::Load(
	    File::AppendPaths(dir, OBJS_FILENAME ),
	    FileModeWriting);

	// -1, since we do not count the car. m_car
	outFile->WriteLine("numObjs " + to_string(m_geoObjs.size()-1) );

	for(auto& it : m_geoObjs) {

	    IGeometryObject* geoObj = it.second;

	    if(geoObj != m_car.get() ) {
		outFile->WriteLine("beginObj");

		outFile->WriteLine("filename " + geoObj->GetFilename() );

		Vector3f p = geoObj->GetPosition();
		outFile->WriteLine("translation " +
				   to_string(p.x) + " " +
				   to_string(p.y) + " " +
				   to_string(p.z)
		    );

		btQuaternion  q = geoObj->GetRotation();
		outFile->WriteLine("rotation " +
				   to_string(q.x() ) + " " +
				   to_string(q.y() ) + " " +
				   to_string(q.z() ) + " " +
				   to_string(q.w() ) + " "

		    );


		float s = geoObj->GetScale();
		outFile->WriteLine("scale " +
				   to_string(s )

		    );

		outFile->WriteLine("endObj");
	    }

	}

	delete outFile;

    }

    LOG_I("cleanup");
}

void TuhuApplication::ParseObjs(const std::string& filename) {

    BufferedFileReader* reader = BufferedFileReader::Load(  filename);
    if(!reader) {
	PrintErrorExit();
    }

    string firstLine = reader->ReadLine();

    size_t numObjs = stoi(StringUtil::SplitString(firstLine, " ")[1]);

    for(size_t iObj = 0; iObj < numObjs; ++iObj) {

	reader->ReadLine(); // beginObj

	string filenameLine = reader->ReadLine();
	string filename = StringUtil::SplitString(filenameLine, " ")[1];

	// read translation.
	vector<string> tokens = StringUtil::SplitString(reader->ReadLine(), " ");
	Vector3f translation = Vector3f(stof(tokens[1]),stof(tokens[2]), stof(tokens[3]) );

	// read rotation
	tokens = StringUtil::SplitString(reader->ReadLine(), " ");
	btQuaternion rotation = btQuaternion(
	    stof(tokens[1]),
	    stof(tokens[2]),
	    stof(tokens[3]),
	    stof(tokens[4]));

	// read scale.
	tokens = StringUtil::SplitString(reader->ReadLine(), " ");
	float scale = stof(tokens[1]);

	reader->ReadLine(); // endObj

	// parsed the object. now add it to the world.
	LoadObj(filename, translation, rotation, scale);

    }
}


void TuhuApplication::TranslationAccepted() {
    m_selected->SetPosition( m_gui->GetTranslation() + m_selected->GetPosition() );
    m_selected->SetEditPosition( Vector3f(0.0f) );
}

void TuhuApplication::RotationAccepted() {
    m_selected->SetRotation(

	m_selected->GetRotation() * toBtQuat(m_gui->GetRotation()));
    m_selected->SetEditRotation( btQuaternion::getIdentity()  );
}

void TuhuApplication::ScaleAccepted() {
    m_selected->SetScale(m_selected->GetScale() * m_gui->GetScale());
    m_selected->SetEditScale( 1.0f  );
}

void TuhuApplication::ModelAdded(const std::string& filename) {

    if(m_selected)
	m_selected->SetSelected(false);

    m_selected = LoadObj(filename, Vector3f(0,0,0) );
    m_selected->SetSelected(true);
}

void TuhuApplication::CursorSizeChanged() {
    m_heightMap->SetCursorSize(m_gui->GetCursorSize());
}

// duplicate selected object in gui editor.
void TuhuApplication::Duplicate() {
    if(m_selected) {

	IGeometryObject* dupObj = m_selected->Duplicate(currentObjId++);

	LOG_I("duplate %d, creating %d", m_selected->GetId(), dupObj->GetId() );

	// select duplicated object:
	m_selected->SetSelected(false);
	dupObj->SetSelected(true);
	m_selected = dupObj;

	m_geoObjs[dupObj->GetId()] = dupObj;
    }
}

// delete selected object in gui editor.
void TuhuApplication::Delete() {
    LOG_I("delete");

    if(m_selected) {

	// not selected anymore.
	m_selected->SetSelected(false);

	LOG_I("delete %d", m_selected->GetId() );


	// delete from map:
	auto it = m_geoObjs.find(m_selected->GetId());
	GeometryObject::Delete(it->second );
	m_geoObjs.erase(it);

	m_selected = NULL;
    }
}

void TuhuApplication::UpdateMatrices() {
    Vector3f carPos = m_car->GetPosition();

    Vector3f carSide = Vector3f::Cross(m_car->GetForwardVector(), Vector3f(0,1,0));

    Vector3f cameraPos = carPos + Vector3f(m_lightDirection);

    Matrix4f lightViewMatrix = Matrix4f::CreateLookAt(
        cameraPos,
        carPos,
        Vector3f(0.0, 1.0, 0.0)
        );

    Config& config = Config::GetInstance();

    float left =-340;
    float right = 345;
    float bottom = -249;
    float top = 150;
    float zNear = -300;
    float zFar = 500;

    Matrix4f lightProjectionMatrix =  //MakeLightProj();

        Matrix4f::CreateOrthographic(
            left,right, // left, right
            bottom, top, // bottom, top
            zNear, // near(front of car)
            zFar); // far(behind car)
//	    Matrix4f::CreateOrthographic(-350,350, -200, 200, -200, 500);

    m_lightVp = lightProjectionMatrix * lightViewMatrix;

    Vector3f min = Vector3f(left, bottom, zNear) + cameraPos;
    Vector3f max = Vector3f(right, top, zFar) + cameraPos;

    Vector3f center = (min + max) * 0.5f;

    Vector3f radius = max - center;

    m_aabbWireframe->SetModelMatrix(
//	    lightViewMatrix *

        Matrix4f::CreateTranslation(center ) *

        Matrix4f::CreateScale(radius )
        );

    m_lightVp = MakeLightProj(GetFramebufferWidth(), GetFramebufferHeight() );
}

void TuhuApplication::BakeAo(int samples, int waveLength, int amplitude, float distAttenuation) {
    m_heightMap->BakeAo(samples, waveLength, amplitude, distAttenuation);
}

void TuhuApplication::LightUpdate() {
    m_lightingPass->UpdateTextures(GeometryObject::GetTorches().size() );
}


void TuhuApplication::BuildRoad() {
    m_heightMap->BuildRoad();
}


void TuhuApplication::DeleteCP() {

    m_heightMap->DeleteCP();

}
