out vec4 fragmentColor;
in vec3 outPos;

void main() {
    fragmentColor = vec4(outPos, 1);
}
