in vec2 texCoord;

out vec4 fragmentColor;



uniform sampler2D positionFbo;
uniform sampler2D vertexPosTexture;
uniform sampler2D fboSource;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform float numSamples;

void main() {

    vec3 vert = texture(vertexPosTexture, texCoord).rgb;

    vert = vec3(viewMatrix * modelMatrix * vec4(vert, 1));

    float z = texture(positionFbo, vert.xy + 0.5).b;

    float uBias = 0.01;


    float o = 0.0;
    if ((vert.z - z) < -uBias) {
        o = 1.0;
    }

    float src = texture(fboSource, texCoord).r;

    o = ((numSamples - 1.0)/numSamples) * src + (1.0/numSamples) * o;

    fragmentColor = vec4( o , 0, 0, 1);
}
