out vec4 fragmentColor;

in vec2 texCoord;

uniform sampler2D tex;

void main(){
    fragmentColor = texture(tex,texCoord);
}
