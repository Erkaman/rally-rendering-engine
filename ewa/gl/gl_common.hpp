#pragma once

#pragma warning( disable : 4127 )

#include <glad/glad.h>


#include <GLFW/glfw3.h> // GLFW helper library

#include <vector>
#include "gl_util.hpp"

typedef std::vector<GLushort> UshortVector;
typedef std::vector<GLfloat> FloatVector;
typedef std::vector<GLuint> UintVector;
