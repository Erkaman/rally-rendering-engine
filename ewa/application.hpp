#pragma once

#include <string>
#include "gl/vao.hpp"

#pragma warning( disable : 4099 )

struct GLFWwindow;
class ShaderProgram;
class Font;

// the main application class.
class Application {

protected:

    GLFWwindow* m_window;
    float m_guiVerticalScale;

private:

    void SetupOpenGL();
    void DoMainLoop();
    void Cleanup_internal();
    void Update_internal(const float delta);
    void RenderText_internal(const std::string& fpsString);

    bool m_running;

    std::unique_ptr<VAO> m_vao;

    int m_width;
    int m_height;

    int m_framebufferWidth;
    int m_framebufferHeight;
public:
    void Start();
    int GetWindowWidth();
    int GetWindowHeight();

protected:

    Application(int argc, char *argv[], int width=800*1.5, int height=600*1.2);

    std::unique_ptr<Font> m_font;
    std::unique_ptr<ShaderProgram> m_fontShader;

    virtual void Render() = 0;
    virtual void Init() = 0;
    virtual void Update(const float delta) = 0;
    virtual void RenderText() {}
    virtual void Cleanup() {}

    void SetViewport();

    void SetWindowTitle(const std::string& title);

    int GetFramebufferWidth();
    int GetFramebufferHeight();
};
