#pragma once

#include "ewa/gl/vbo.hpp"

#include "ewa/math/vector3f.hpp"

class ShaderProgram;
class Matrix4f;

// class for drawing lines. useful for debugging.
class Line {

private:

    std::unique_ptr<VBO> m_vertexBuffer;
    std::unique_ptr<ShaderProgram> m_shader;

    // line start point.
    Vector3f m_start;

    // line end point.
    Vector3f m_end;

    Vector3f m_color;

    Line(const Vector3f& start, const Vector3f& end, const Vector3f& color, ShaderProgram* shader);

public:

    static Line* Load(const Vector3f& start, const Vector3f& end, const Vector3f& color = Vector3f(1.0f,1.0f,0.0f) );
    ~Line();

    void SetStartEnd(const Vector3f& start, const Vector3f& end);
    void SetStart(const Vector3f& start);
    void SetEnd(const Vector3f& end);
    void SetColor(const Vector3f& color);

    void Render(const Matrix4f& vp);
};
